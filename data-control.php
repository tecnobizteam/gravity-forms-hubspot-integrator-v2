<?php
require_once( str_replace('//','/',dirname(__FILE__).'/') .'../../../wp-config.php');

global $wpdb;

if ('associacoes' == $_REQUEST['table'])
{
	$table_name = $wpdb->prefix . "gf_hubspot_integrator_2";

	switch ($_SERVER['REQUEST_METHOD']) {
		case 'POST':
			$form_id = $_POST['id_gravity_form'];
			$field = $_POST['id_gravity_field'];

			$count = $wpdb->get_results("SELECT * FROM $table_name WHERE id_gravity_form = $form_id AND id_gravity_field = $field;");

			if (count($count) > 0){
				break;
			}

			$array = array(
				'id_gravity_form' 	=> $form_id,
				'id_gravity_field' 	=> $field,
				'hubspot_form_id' 	=> $_POST['hubspot_form_id'],
				'hubspot_field_name' 	=> $_POST['hubspot_field_name'],
			);

			$bool = $wpdb->insert(
				$table_name,
				$array
			);

			$lastId = $wpdb ->insert_id;

			$arrayResult = array(
				"return" => $bool,
				"id" => $lastId,
			);

			echo json_encode($arrayResult);
			break;

		case 'GET':
			if ( $_GET['action'] == 'delete' && $_GET['id'] ) {
				$boolDel = $wpdb->delete( $table_name, array( 'id' => $_GET['id'] ) );

			}
			echo $boolDel;
			break;

		default:
			# code...
			break;
	}
}

if ('urlmap' == $_REQUEST['table'])
{
	$table_name = $wpdb->prefix . "gf_hubspot_url_map";

	switch ($_SERVER['REQUEST_METHOD']) {
		case 'POST':
			$wpdb->insert(
				$table_name,
				array(
					'url'   => $_POST['url'],
					'regex' => $_POST['regex'] ? true : false,
					'value' => $_POST['value'],
				)
			);
			delete_transient( 'hubspot_url_map' );
			break;

		case 'GET':
			if ( $_GET['action'] == 'delete' && $_GET['id'] ) {
				$wpdb->delete( $table_name, array( 'id' => $_GET['id'] ) );
			}
			delete_transient( 'hubspot_url_map' );

			break;
		default:
			# code...
			break;
	}
	$redirect = $_SERVER["HTTP_REFERER"];
	header("location:$redirect");
}