<?php

require_once('../gravityforms/includes/api.php');

$form_id = $_GET['id'];

$fields = GFAPI::get_form( $form_id );
$fields = $fields['fields'];

$options = '';
foreach( $fields as $field ):
	$options .= '<option value="' . $field->id . '">' . $field->label . '</option>';
endforeach;

echo $options;