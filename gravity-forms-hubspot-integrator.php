<?php

/*
Plugin Name: Gravity Forms HubSpot Integrator
Description: Envia leads do Gravity Forms para o HubSpot
Version: 2.0
License: GPL
Author: Raphael Batagini @ Tecnobiz
Author: Fabio de Godoy @ Tecnobiz
Author URI: 
*/

// create custom plugin settings menu
add_action('admin_menu', 'gf_HubSpot::add_hubspot_integrator_menu');

add_action( 'gform_after_submission', 'gf_HubSpot::send_to_hubspot', 10, 2 );

add_action( 'admin_footer', 'gf_HubSpot::load_gravity_form_fields' ); // Write our JS below here

add_action( 'wp_ajax_get_gravity_form_fields', 'gf_HubSpot::get_gravity_form_fields' );

add_action( 'wp_ajax_nopriv_get_gravity_form_fields', 'gf_HubSpot::get_gravity_form_fields' );

register_activation_hook( __FILE__, 'gf_HubSpot::create_database' );

class gf_HubSpot
{

    function add_hubspot_integrator_menu()
    {
        //create new top-level menu
        add_menu_page('Gravity Forms HubSpot Integrator', 'GF HubSpot Integrator', 'administrator', __FILE__, 'gf_HubSpot::gf_integrator_settings_page', plugins_url('/images/icon.png', __FILE__));

        add_submenu_page(__FILE__, 'Associações', 'Associações', 'administrator', 'sub_menu_associations_hubspot', 'gf_HubSpot::sub_menu_associations_page');

        add_submenu_page(__FILE__, 'URL -> Nome', 'URL -> Nome', 'administrator', 'sub_menu_urlmap_hubspot', 'gf_HubSpot::sub_menu_urlmap_page');

        add_submenu_page(__FILE__, 'Logs', 'Logs', 'administrator', 'sub_menu_logs_hubspot', 'gf_HubSpot::sub_menu_logs');

        //call register settings function
        add_action('admin_init', 'gf_HubSpot::register_mysettings');
    }

    function gf_integrator_settings_page()
    {
        ?>
        <div class="wrap">
            <h1>Gravity Forms HubSpot WS Integrator</h1>
            <form method="post" action="options.php">
			    <?php settings_fields( 'gravity-forms-hubspot-integrator-group' ); ?>
			    <?php do_settings_sections( 'gravity-forms-hubspot-integrator-group' ); ?>
                <br>
                <label for="ativa_hubspot">
                    <input name="ativa_hubspot" value="1" type="checkbox" <?php echo (get_option('ativa_hubspot') == '1') ? 'checked' : ''; ?> >
                    Integra com HubSpot
                </label>
                <h2>Cadastrar Portal ID/ Hubspot ID</h2>
                <input type="text" name="api_key_option" value="<?php echo get_option('api_key_option'); ?>" placeholder="Hubspot ID" />
                <h2>Campo para nome de formulário no HubSpot</h2>
                <input type="text" name="api_form_name" value="<?php echo get_option('api_form_name'); ?>" placeholder="Formulário no HubSpot" />
			    <?php submit_button(); ?>
            </form>
        </div>
        <?php
    }

    function sub_menu_associations_page()
    {
        ?>
        <style>
            .message{
                display: none;
            }
        </style>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.3/jquery-confirm.min.js"></script>
        <script type="text/javascript" >
            jQuery(document).ready(function($) {
                $('#new-field').on('submit', function (e) {
                    e.preventDefault();
                    var message = message = document.querySelector(".message");
                    var dados = jQuery(this).serialize();

                    $.ajax({
                        method: "POST",
                        url: '<?php echo plugins_url('/data-control.php', __FILE__); ?>',
                        data: dados,
                        dataType: 'json',
                        beforeSend: function(){
                            $("#id_gravity_form").prop('disabled', true);
                            $("#id_gravity_field").prop('disabled', true);
                            $("input[type=text][name=hubspot_field_name]").prop('disabled', true);
                            $("input[type=text][name=hubspot_form_id]").prop('disabled', true);
                            $('.spinner').addClass('is-active');
                            message.style.display = "none";
                            message.textContent = "";
                        },
                        complete: function(){
                            $('#id_gravity_form').prop('disabled', false);
                            $("#id_gravity_field").prop('disabled', false);
                            $("input[type=text][name=hubspot_field_name]").prop('disabled', false);
                            $("input[type=text][name=hubspot_form_id]").prop('disabled', false);
                            $('.spinner').removeClass('is-active');
                            message.style.display = "inline-block";
                        },
                        success: function(data) {
                            if (data === null) {
                                message.textContent = "Associação já existe, por favor remova a anterior!";
                            } else {
                                message.textContent = "Associação realizada com sucesso";

                                var $pathPlugin = '<?php echo plugins_url("/", __FILE__)  ?>';

                                // Pega os valores dos selects
                                var form = $("#id_gravity_form option:selected").text();
                                var campo = $("#id_gravity_field option:selected").text();
                                var campoInput = $("input[type=text][name=hubspot_field_name]").val();
                                var campoInputID = $("input[type=text][name=hubspot_form_id]").val();

                                // Cria os DOM's
                                var $tr = document.createElement('tr');
                                var $td = document.createElement('td');
                                var $td2 = document.createElement('td');
                                var $td3 = document.createElement('td');
                                var $td4 = document.createElement('td');
                                var $tdID = document.createElement('td');
                                var $a = document.createElement('a');

                                // Popula o link de remover
                                $a.setAttribute('id', 'btn-remover');
                                $a.setAttribute('href', $pathPlugin + 'data-control.php?action=delete&id=' + data.id + '&table=associacoes');
                                $a.textContent = 'Remover';
                                $td4.append($a);

                                $td.textContent = form;
                                $td2.textContent = campo;
                                $td3.textContent = campoInput;
                                $tdID.textContent = campoInputID;

                                $tr.append($td, $td2, $tdID, $td3, $td4);
                                $('#tableAssociation tbody').append($tr);
                            }
                            setTimeout(function () {
                                message.style.display = "none";
                            }, 3000);
                        },
                        error: function(){
                            message = document.querySelector(".message");
                            message.textContent = "Erro! Não foi possível realizar a associação!";
                            message.style.display = "inline-block";

                            setTimeout(function(){ message.style.display = "none"; }, 3000);
                        }
                    });
                });
                $('body').on('click', '#btn-remover', function(e) {
                    e.preventDefault();

                    // Referencia o link do botao e a td
                    var $link       = $(this);
                    var $linkHref   = $link.attr("href");
                    var $tdLink     = $link.parent();

                    //  Cria o botao de sim
                    var $apagar = createButton(1,"Sim", $linkHref);

                    // Cria o botao nao
                    var $cancelar = createButton(0,"Não", $linkHref);

                    // some com o link e adiciona os botoes de confirmacao
                    $link.hide();
                    $tdLink.append($apagar);
                    $tdLink.append($cancelar);
                });

                function createButton(value, text, link) {
                    var $button = document.createElement('button');
                    $button.setAttribute('class', 'confirmar');
                    $button.setAttribute('data-href', link);
                    $button.setAttribute('data-option', value);
                    $button.setAttribute('type', 'button');
                    $button.textContent = text;

                    return $button;

                }

                $('body').on('click', '.confirmar', function() {
                    var $link = $(this).data('href');
                    var $resposta = $(this).data('option');
                    var $td = $(this).parent();
                    var $tr = $td.parent();

                    if ($resposta == "1"){
                        var $return = removeRegistro($link, $tr);
                    }else{

                        var $button = $td.find(".confirmar");
                        var $linkA = $td.find("#btn-remover");

                        $button.remove();
                        $linkA.show();
                    }
                });


                function removeRegistro (link, tr){
                    $.ajax({
                        method: "GET",
                        url: link,
                        dataType: 'text',
                        success: function (data) {
                            if(data == 1){
                                tr.remove();
                            }else{
                                var message = document.querySelector(".message");
                                message.textContent = "Erro! Não foi possível remover a associação";
                                message.style.display = "inline-block";

                                setTimeout(function(){ message.style.display = "none"; }, 3000);

                            }
                        },
                        error: function () {
                        }
                    });
                }
            });
        </script>
        <div class="wrap">
            <h1>Gravity Forms HubSpot Integrator</h1>
            <h2>Criar nova associação</h2>
            <form method="POST" id="new-field" action="<?php echo plugins_url('/data-control.php', __FILE__); ?>">
                <input type='hidden' name="table" value="associacoes" />
                <table>
                    <tr>
	                    <?php
                            //print select form
                            $select = '<select name="id_gravity_form" id="id_gravity_form" style="width:100%">';
                            $forms = RGFormsModel::get_forms( null, 'title' );
                            foreach( $forms as $form ):
                                $select .= '<option value="' . $form->id . '">' . $form->title . '</option>';
                            endforeach;
                            $select .= '</select>';
                            echo "<td>Formulário</td><td>$select</td>";
	                    ?>
                    </tr>
                    <tr>
                        <td>Campo</td>
                        <td>
                            <select name="id_gravity_field" id="id_gravity_field" style="width:100%">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Form ID Hubspot</td>
                        <td><input type="text" name="hubspot_form_id" placeholder="Formulário HubSpot" style="width:100%"/></td>
                    </tr>
                    <tr>
                        <td>Campo HubSpot</td>
                        <td><input type="text" name="hubspot_field_name" placeholder="Nome HubSpot" style="width:100%"/></td>
                    </tr>
                </table>
                <div class="spinner" style="float:none;width:auto;height:auto;padding: 0px 0 10px 40px;/* background-position:20px 0; */">Buscando campos do formulário</div>
                <div class="message" style="float:none;width:auto;height:auto;padding: 0px 0 10px 40px;/* background-position:20px 0; */"></div>
                <br/>
                <button id="btn_association" type="submit" class="button button-primary">Salvar</button>
            </form>
            <br/>

	        <?php
                global $wpdb;
                $table_name = $wpdb->prefix . "gf_hubspot_integrator_2";
                $associations = $wpdb->get_results(
                    "
                    SELECT id, id_gravity_form, id_gravity_field, hubspot_field_name, hubspot_form_id
                    FROM $table_name
                    ORDER BY id_gravity_form
                    "
                );
	        ?>

            <h2>Associações</h2>
            <table id="tableAssociation" class="wp-list-table widefat fixed striped posts">
                <thead>
                <tr>
                    <th>Formulário</th>
                    <th>Campo GF</th>
                    <th>Form ID Hubspot</th>
                    <th>Campo HubSpot</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
		        <?php
		        foreach ( $associations as $association ):
			        ?>
                    <tr>
                        <td>
	                        <?php
                                $idGFShow = GFAPI::get_form($association->id_gravity_form);
                                echo $idGFShow['title'];
	                        ?>
                        </td>
                        <td>
	                        <?php
                                $fields = GFAPI::get_form($association->id_gravity_form);
                                $fields = $fields['fields'];
                                foreach ($fields as $field):
                                    if ($association->id_gravity_field == $field->id) {
                                        echo $field->label;
                                        continue;
                                    }
                                endforeach;
	                        ?>
                        </td>
                        <td>
					        <?php echo $association->hubspot_form_id; ?>
                        </td>
                        <td>
					        <?php echo $association->hubspot_field_name; ?>
                        </td>
                        <td>
                            <a id="btn-remover" href="<?php echo plugins_url('/data-control.php?action=delete&id=' . $association->id, __FILE__); ?>&table=associacoes">Remover</a>
                        </td>
                    </tr>
			        <?php
		        endforeach;
		        ?>
                </tbody>
            </table>
        </div>
        <?php

    }

    function sub_menu_urlmap_page()
    {
        ?>
        <div class="wrap">
            <h1>Gravity Forms HubSpot Integrator</h1>
            <h2>Criar novo mapeamento</h2>
            <form method="POST" id="new-map" action="<?php echo plugins_url('/data-control.php', __FILE__); ?>">
                <input type='hidden' name="table" value="urlmap" />
                <table style="min-width:750px;max-width:100%;">
                    <tr>
                        <td style="width:110px;">URL</td>
                        <td style="position:relative;">
                            <span id='baseurl' style="position:absolute;top:calc(50% - 7px);font-size:14px;line-height:15px;left:10px;color:#000066;"><?php echo site_url(); ?></span>
                            <input id='url' type="text" name="url" style="width:100%" />
                        </td>
                    </tr>
                    <tr>
                        <td>Expressão regular?</td>
                        <td>
                            <input id='regex' type='checkbox' name='regex' />
                        </td>
                    </tr>
                    <tr>
                        <td>Mapear para</td>
                        <td>
                            <input type="text" name="value" placeholder="Nome" style="width:100%"/>
                        </td>
                    </tr>
                </table>
                <br />
                <button type="submit" class="button button-primary">Salvar</button>
            </form>
            <br/>
	        <?php
                global $wpdb;
                $table_name = $wpdb->prefix . "gf_hubspot_url_map";
                $urlmap = $wpdb->get_results(
                    "
                SELECT *
                FROM $table_name
                ORDER BY id
                "
                );
	        ?>
            <h2>Mapeamentos de URL</h2>
            <table class="wp-list-table widefat fixed striped posts">
                <thead>
                <tr>
                    <th>URL</th>
                    <th>Expressão regular?</th>
                    <th>Mapeia para</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ( $urlmap as $map ) :
                    ?>
                    <tr>
                        <td>
                            <?php echo $map->url; ?>
                        </td>
                        <td>
                            <?php echo $map->regex ? 'Sim' : 'Não'; ?>
                        </td>
                        <td>
                            <?php echo $map->value; ?>
                        </td>
                        <td>
                            <a href="<?php echo plugins_url('/data-control.php?action=delete&id=' . $map->id, __FILE__); ?>&table=urlmap">Remover</a>
                        </td>
                    </tr>
                    <?php
                endforeach;
                ?>
                </tbody>
            </table>
            <script type='text/javascript'>
                (function($)
                {
                    $(document).on('ready', function()
                    {
                        $('#url').css('padding-left', $('#baseurl').width() + 11);
                    });
                    $('#url').on('change', function()
                    {
                        if ($('#url').val().indexOf('<?php echo site_url(); ?>') == 0)
                        {
                            $('#url').val($('#url').val().replace('<?php echo site_url(); ?>', ''));
                        }
                    });
                })(jQuery);
            </script>
        </div>
        <?php
    }

    //logs e exporta logs
    function sub_menu_logs()
    {
	    global $wpdb;

	    $table_log = $wpdb->prefix . "gf_hubspot_log_2";

	    $pagenum = isset($_GET['pagenum']) ? absint($_GET['pagenum']) : 1;

	    $limit = 50; // number logs per page
	    $offset = ( $pagenum - 1 ) * $limit;
	    $total = $wpdb->get_var("SELECT COUNT(`id`) FROM $table_log");
	    $num_of_pages = ceil($total / $limit);


	    $page_links = paginate_links(array(
		    'base' => add_query_arg('pagenum', '%#%'),
		    'format' => '',
		    'prev_text' => __( '&laquo;', 'text-domain' ),
		    'next_text' => __( '&raquo;', 'text-domain' ),
		    'total' => $num_of_pages,
		    'current' => $pagenum
	    ));

	    $query = "SELECT * FROM	$table_log";

	    $total = $wpdb->get_var($total);
	    $items_per_page = $limit;
	    $page = isset($_GET['pagenum']) ? abs((int) $_GET['pagenum']) : 1;
	    $offset = ( $page * $items_per_page ) - $items_per_page;
	    $logs = $wpdb->get_results($query . " ORDER BY data DESC LIMIT ${offset}, ${items_per_page}");

        ?>

        <div id="wpbody">
            <div class="wpbody-content">
                <div class="wrap">
                    <!-- a href="<?php echo admin_url( 'admin-post.php?action=print_csv_logs.csv' ); ?>" style="float: left" class="button-primary revgreen">Exportar Log</a -->
				    <?php
				    if ($page_links) {
					    echo '<div class="tablenav" style="clear: none;"><div class="tablenav-pages">' . $page_links . '</div></div>';
				    }
				    ?>
                    <br>
				    <?php
				    echo "<table class='wp-list-table widefat fixed pages'>";
				    echo "<thead>";
				    echo '<tr>';
				    echo '<td>Serviço</td>';
				    echo '<td>Email</td>';
				    echo '<td>Dados enviados</td>';
				    echo '<td>Data</td>';
				    echo '<td>Status</td>';
				    echo '<td>Detalhes</td>';
				    echo '</tr>';
				    echo "</thead>";
				    echo '<tbody class="the-list">';

				    foreach ($logs as $log) {
					    echo "<tr>";
					    echo "<td>".$log->servico."</td>";
					    echo "<td>".$log->email."</td>";
					    echo "<td>".$log->dados_enviados."</td>";
					    echo "<td>".$log->data."</td>";
					    echo "<td>".$log->status."</td>";
					    echo "<td>".$log->detalhes."</td>";
					    echo "</tr>";
				    }
				    echo "</tbody>";
				    echo "</table>";
				    ?>
                </div>
            </div>
        </div>
        <?php
    }

	function register_mysettings() { // whitelist options
		register_setting( 'gravity-forms-hubspot-integrator-group', 'api_key_option' );
		register_setting( 'gravity-forms-hubspot-integrator-group', 'api_form_name' );
		register_setting( 'gravity-forms-hubspot-integrator-group', 'ativa_hubspot' );
		register_setting( 'gravity-forms-hubspot-integrator-group', 'ws_endpoint' );
	}

	function send_to_hubspot($entry, $form)
	{
		setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
		date_default_timezone_set('America/Sao_Paulo');

		global $wpdb;

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		$arr = gf_HubSpot::format_form_properties($entry, $form);

		if (!$arr) {
			return;
		}

		$str_post = '';

		$nome_produto_composto = '';

		foreach ($arr['properties'] as $property) {

			$str_post .= $property['property']."=".$property['value']."&";

			//seta as variáveis para o ws

			if ($property['property'] == 'email') {
				$id_form = $property['form_hub_id'];
				$nome_produto = $property['nome_produto'];
				$email = $property['value'];
			}

			if (
				$property['property'] == 'firstname' ||
				$property['property'] == 'nome'
			) {
				$nome = $property['value'];
			}

			if (
				$property['property'] == 'phone' ||
				$property['property'] == 'telefone'
			) {
				$telefone = gf_HubSpot::clear_fields($property['value']) ;
			}

			if ($property['property'] == 'cnpj') {
				$cnpj = gf_HubSpot::clear_fields($property['value']);
			}

			if ($property['property'] == 'plano') {
				$nome_produto_composto = $nome_produto." ".$property['value'];
			}

			if ($property['property'] == 'valor') {
				$nome_produto_composto = $nome_produto." ".$property['value'];
			}

		}

		if ($nome_produto_composto) {
			$nome_produto = $nome_produto_composto;
		}

		$portal_id = get_option('api_key_option');

		if(get_option('ativa_hubspot')){

			$endpoint = 'https://forms.hubspot.com/uploads/form/v2/'.$portal_id.'/'.$id_form;

			//envio hubspot
			$hubspotutk      = $_COOKIE['hubspotutk']; //grab the cookie from the visitors browser.
			$ip_addr         = $_SERVER['REMOTE_ADDR']; //IP address too.
			$hs_context      = array(
				'hutk' 				=>		 $hubspotutk,
				'first_page_seen'	=>		 $_COOKIE['__hstc'],
				'last_page_seen'	=>		 $_COOKIE['__hstc'],
				'ipAddress' 		=> 		 $ip_addr,
				'pageUrl' 			=>		 get_permalink(),
				'pageName' 			=>	 	 html_entity_decode(get_the_title())
			);
			$hs_context_json = json_encode($hs_context);

			$dados_enviados = $str_post;

			$str_post .= "hs_context=".urlencode($hs_context_json);

			$ch = @curl_init();
			@curl_setopt($ch, CURLOPT_POST, true);
			@curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
			@curl_setopt($ch, CURLOPT_URL, $endpoint);
			@curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/x-www-form-urlencoded'
			));
			@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response    = @curl_exec($ch); //Log the response from HubSpot as needed.
			$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE); //Log the response status code
			@curl_close($ch);

            $params = array(
				'email' 			=> $email,
				'data' 				=> date('Y-m-d H:i:s'),
				'dados_enviados' 	=> json_encode($dados_enviados),
				'servico' 			=> 'Hubspot',
				'status' 			=> $status_code,
				'detalhes' 			=> $response,
				'ip' 				=> $ip,
			);

            $wpdb->insert($wpdb->prefix . 'gf_hubspot_log_2', $params );
		}
	}

    function clear_fields($valor){
        $valor = trim($valor);
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", "", $valor);
        $valor = str_replace("-", "", $valor);
        $valor = str_replace("/", "", $valor);
        $valor = str_replace("(", "", $valor);
        $valor = str_replace(")", "", $valor);
        $valor = str_replace(" ", "", $valor);
        return $valor;
    }

	function search_user($email) {
		$endpoint = 'https://api.hubapi.com/contacts/v1/contact/email/' . $email . '/profile?hapikey=' . get_option('api_key_option');
		$user = file_get_contents($endpoint);
		return $user;
	}

    //format entry data into form
    function format_form_properties($entry, $form)
    {
	    global $wpdb;
	    $table_name = $wpdb->prefix . "gf_hubspot_integrator_2";
	    $form_id = $entry['form_id'];
	    $api_form_name_set = false;

	    $form_values = array();
	    foreach ($form['fields'] as $key => $field) {
		    $hubspot_field_name = $wpdb->get_var("
	   		SELECT hubspot_field_name
	   		FROM $table_name
	   		WHERE id_gravity_form = $form_id AND id_gravity_field = {$field['id']}
   		");

		    $hubspot_form_id = $wpdb->get_var("
	   		SELECT hubspot_form_id
	   		FROM $table_name
	   		WHERE id_gravity_form = $form_id AND id_gravity_field = {$field['id']}
   		");

		    $urlmap = gf_HubSpot::get_url_mapping($_SERVER['REQUEST_URI']);

		    if (!empty($urlmap)) {
			    $nome_produto = $urlmap;
		    } else {
			    $nome_produto = $_SERVER['REQUEST_URI'];
		    }

		    if($field['type'] === 'checkbox' ){
			    $value_checkox = $entry[$field['id'] .'.1'];
		        if($value_checkox === ''){
			        $value_selected = 'No';
                }else{
			        $value_selected = 'Yes';
                }
			    $form_values[] = array(
				    'property' 		=>	$hubspot_field_name,
				    'form_hub_id'	=>	$hubspot_form_id,
				    'nome_produto'	=>  $nome_produto,
				    'isCheckbox' => 'Yes',
				    'value' 		=>	$value_selected,
			    );
            }else if($field['type'] === 'list'){
			    $repeater_values = $entry[$field['id']];
			    $array_repeater_values = unserialize($repeater_values); //Pega conteúdo que está entres aspas
                $new_value = ''; //Receberá o novo registro concatenado

                if($form['title'] === 'Liberar Audiências Digitais'){
	                foreach ($array_repeater_values as $value) {
		                $number_facebook = $value['Nº da conta do Facebook'];
		                $name_company = $value['Nome da empresa'];
		                if(array_key_exists ('Nº da conta do Facebook', $value ) &&
		                   array_key_exists ('Nº da conta do Facebook', $value ) ){
			                $new_value .= 'Nome da Empresa: ' .$name_company .' | Nº da conta do Facebook: ' .$number_facebook.' | ';
		                }else{
			                $new_value .= ' | Nº: '. $value . ' |';
		                }
	                }
                }else{
	                foreach ($array_repeater_values as $value) {
                        $new_value .= ' | '. $value . ' |';
	                }
                }


			    $new_register = $new_value;

			    $form_values[] = array(
				    'property' 		=>	$hubspot_field_name,
				    'form_hub_id'	=>	$hubspot_form_id,
				    'nome_produto'	=>  $nome_produto,
				    'isRepeater' => 'Yes',
				    'value' 		=>	 $new_register,
			    );
            }
            else{
			    $form_values[] = array(
				    'property' 		=>	$hubspot_field_name,
				    'form_hub_id'	=>	$hubspot_form_id,
				    'nome_produto'	=>  $nome_produto,
				    'value' 		=>	$entry[$field['id']]
			    );
            }
	    }

        if(!$form_values) {
            return false;
        }

        if (!$api_form_name_set && get_option('api_form_name')) {
            $idGFGetTitle = GFAPI::get_form( $form_id );
            $form_values[] = array(
                'property' 	=> get_option('api_form_name'),
                'value' 	=> $idGFGetTitle['title'] . ' - ' . get_the_title()
            );
        }

        return array('properties' => $form_values);
    }

    function get_url_mapping($url)
    {
        if (false === ($maps = get_transient('hubspot_url_map')))
        {
            $maps = gf_HubSpot::get_all_mappings();
            set_transient('hubspot_url_map', $maps, WEEK_IN_SECONDS);
        }

        $result = false;
        foreach ($maps as $map)
        {
            $test = $map->url;
            if ($map->regex)
            {
                if (preg_match("|{$test}|i", $url))
                {
                    $result = $map->value;
                    break;
                }
            }
            else if (false !== stripos($url, $test))
            {
                $result = $map->value;
                break;
            }
        }
        return $result;
    }

    function get_all_mappings()
    {
	    global $wpdb;
	    $table_name = $wpdb->prefix . "gf_hubspot_url_map";
	    return $wpdb->get_results("SELECT * FROM {$table_name}");
    }

    function create_database()
    {
	    global $wpdb;
	    $table_main = $wpdb->prefix . "gf_hubspot_integrator_2";
	    $table_map  = $wpdb->prefix . "gf_hubspot_url_map";
	    $table_log  = $wpdb->prefix . "gf_hubspot_log_2";

        $charset_collate = $wpdb->get_charset_collate();

	    $charset_collate = $wpdb->get_charset_collate();

	    $sql = "CREATE TABLE {$table_main} (
	  id MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
	  id_gravity_form INT(11),
	  id_gravity_field INT(11),
	  hubspot_field_name VARCHAR(255),
	  hubspot_form_id VARCHAR(255),
	  PRIMARY KEY  (id)
	) {$charset_collate};
	CREATE TABLE {$table_map} ( 
	  id INT(9) NOT NULL AUTO_INCREMENT,
	  url VARCHAR(255) NOT NULL,
	  regex BOOLEAN NOT NULL DEFAULT FALSE,
	  value VARCHAR(255) NOT NULL,
	  PRIMARY KEY (id)
	) {$charset_collate};;
	CREATE TABLE {$table_log} ( 
	  id INT(9) NOT NULL AUTO_INCREMENT,
	  email VARCHAR(255) NOT NULL,
	  dados_enviados tinytext,
	  servico VARCHAR(255) NOT NULL,
	  status VARCHAR(255) NOT NULL,
	  detalhes tinytext,
	  data datetime NOT NULL,
	  ip VARCHAR(255) NOT NULL,
	  PRIMARY KEY (id)
	) {$charset_collate};";

	    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	    dbDelta( $sql );
    }

    function load_gravity_form_fields() { ?>
        <script type="text/javascript" >
            jQuery(document).ready(function($) {

                $('#id_gravity_form').on('change', function(){

                    $('.spinner').addClass('is-active');
                    $('#new-field select').prop('disabled', true);
                    $('#new-field input').prop('disabled', true);

                    var formId = $('select#id_gravity_form option:checked').val();

                    var data = {
                        'action' : 'get_gravity_form_fields',
                        'post_type' : 'get',
                        'id' : formId
                    }

                    $.ajax({
                        method: "GET",
                        url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
                        data: data,
                        success: function(data)
                        {
                            $('#id_gravity_field').html(data);
                            $('#new-field select').prop('disabled', false);
                            $('#new-field input').prop('disabled', false);
                            $('.spinner').removeClass('is-active');
                        }
                    })
                });

                $('#id_gravity_form').trigger('change');
            });
        </script> <?php
    }


    function get_gravity_form_fields() {
	    $form_id = $_GET['id'];

	    $fields = GFAPI::get_form( $form_id );
	    $fields = $fields['fields'];

	    $options = '';
	    foreach( $fields as $field ):
		    $options .= '<option value="' . $field->id . '">' . $field->label . '</option>';
	    endforeach;

	    echo $options;
	    die;
    }

}