# Gravity Forms HubSpot Integrator

[![N|Solid](https://www.serasaexperian.com.br/wp-content/themes/serasaexperian/images/logo.png)](https://www.serasaexperian.com.br/)

Este plugin tem a funcionalidade de integrar o Gravity Forms com o HubSpot

# As funcionalidades deste plugin incluem
  - Integrar HubSpot com algum portal por meio do ID
  - Criar associação entre campos do Gravity Forms com campos do HubSpot
  - Mapeamento das urls do site
  - Relatório de Logs para mostrar: Dados enviados, Status Code, Data e E-mail.
  - Formartar dados do gravity para que hubspot receba campos como: Listas e checkcbox

# Instalação
  - Adicionar o arquivo 'gravity-forms-hubspot-integrator-v2.zip' à pasta '/wp-content/plugins/'.
  - Descompactar o arquivo e ativar o plugin.


